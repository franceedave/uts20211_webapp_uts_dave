from django.db import models

# Create your models here.
class Sampah_Nabung(models.Model):
    name = models.CharField(max_length=50)
    type = models.CharField(max_length=50)
    jenis_sampah = [
        ('Plastik', 'Plastik'),
        ('Metal', 'Metal'),
        ('Kertas', 'Kertas'),
    ]
    sampah = models.CharField(
        max_length=20,
        choices=jenis_sampah,
    )

    def __str__(self):
        return self.name

class user(models.Model):
    username = models.CharField(max_length= 50)
    user_id = models.IntegerField(max_length= 100)
    

    def __str__(self) :
        return self.username

class setor(models.Model):
    status_setor = models.BooleanField(default=True)
    user_id = models.IntegerField(max_length=50)
    

    def __str__(self):
        return self.status_setor